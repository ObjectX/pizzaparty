//
//  PizzaRestaurantDetailsViewController.m
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import "PizzaRestaurantDetailsViewController.h"

@interface PizzaRestaurantDetailsViewController ()

@end

@implementation PizzaRestaurantDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self reloadUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setRestaurant:(Restaurant *)restaurant
{
    _restaurant = restaurant;
    [self reloadUI];
}

#pragma mark - Utility methods

-(void)reloadUI
{
    self.nameLabel.text = self.restaurant.name;
    self.phoneLabel.text = self.restaurant.formattedPhone;
    self.addressLabel.text = self.restaurant.formattedAddress;
}

@end
