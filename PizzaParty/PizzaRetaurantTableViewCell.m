//
//  PizzaRetaurantTableViewCell.m
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import "PizzaRetaurantTableViewCell.h"

@interface PizzaRetaurantTableViewCell()

@property (strong, nonatomic) NSNumberFormatter * floatFormat;

@end

@implementation PizzaRetaurantTableViewCell

static NSNumberFormatter * formatter = nil;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setRestaurant:(Restaurant *)restaurant
{
    _restaurant = restaurant;
    self.nameLabel.text = _restaurant.name;
    float distance = _restaurant.distance.floatValue / 1609.344f;
    self.distanceLabel.text = [NSString stringWithFormat:@"%@ miles",[self.floatFormat stringFromNumber:[NSNumber numberWithFloat:distance]]];
}

-(NSNumberFormatter *)floatFormat
{
    if(formatter == nil)
    {
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    }
    return  formatter;
}
@end
