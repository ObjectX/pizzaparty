//
//  Restaurant.m
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import "Restaurant.h"


@implementation Restaurant

@dynamic name;
@dynamic formattedPhone;
@dynamic lat;
@dynamic lng;
@dynamic distance;
@dynamic formattedAddress;

@end
