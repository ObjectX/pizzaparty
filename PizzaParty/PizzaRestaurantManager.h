//
//  PizzaRestaurantManager.h
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^Callback) (void);

@interface PizzaRestaurantManager : NSObject <CLLocationManagerDelegate>

+(PizzaRestaurantManager*)instance;

-(void)loadNearbyRestaurants;

-(NSFetchedResultsController *)getRestaurantsAsync:(Callback)callback;
@end
