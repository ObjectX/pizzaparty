//
//  PizzaRestaurantsViewController.m
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import "PizzaRestaurantsViewController.h"
#import "Constants.h"
#import "PizzaRestaurantDetailsViewController.h"
#import "PizzaRestaurantManager.h"
#import "PizzaRetaurantTableViewCell.h"

#define PIZZA_RETAURANT_CELL @"PizzaRetaurantTableViewCell"

@interface PizzaRestaurantsViewController ()

@property (strong, nonatomic) Restaurant * selectedRestaurant;
@property (strong, nonatomic) NSFetchedResultsController * restaurants;

@end

@implementation PizzaRestaurantsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UINib * nib = [UINib nibWithNibName:PIZZA_RETAURANT_CELL bundle:NULL];
    [self.tableView registerNib:nib forCellReuseIdentifier:PIZZA_RETAURANT_CELL];
    
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(restaurantsLoaded:) name:RestaurantsLoadedNotification object:nil];
    [center addObserver:self selector:@selector(restaurantsLoadingFailed:) name:RestaurantsLoadingFailure object:nil];
    
    [[PizzaRestaurantManager instance] loadNearbyRestaurants];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:PizzaRestaurantDetailsSegue])
    {
        PizzaRestaurantDetailsViewController * vc = (PizzaRestaurantDetailsViewController *)[segue destinationViewController];
        vc.restaurant = self.selectedRestaurant;
    }
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.restaurants.fetchedObjects.count;
    return  count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    PizzaRetaurantTableViewCell *cell = (PizzaRetaurantTableViewCell *)[tableView dequeueReusableCellWithIdentifier:PIZZA_RETAURANT_CELL forIndexPath:indexPath];
    cell.restaurant = [self restaurantForIndexPath:indexPath];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedRestaurant = [self restaurantForIndexPath:indexPath];
    [self performSegueWithIdentifier:PizzaRestaurantDetailsSegue sender:self];
}

#pragma mark - Notifications

-(void)restaurantsLoaded:(NSNotification *)notifications
{
    self.restaurants = [[PizzaRestaurantManager instance] getRestaurantsAsync:^{
        [self.tableView reloadData];
    }];
}

-(void)restaurantsLoadingFailed:(NSNotification *)notifications
{
    self.restaurants = nil;
}

#pragma mark - Utility methods
-(Restaurant*)restaurantForIndexPath:(NSIndexPath*)indexPath
{
    Restaurant * restaurant = [self.restaurants objectAtIndexPath:indexPath];
    return restaurant;
}

@end
