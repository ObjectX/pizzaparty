//
//  PizzaRetaurantTableViewCell.h
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Restaurant.h"

@interface PizzaRetaurantTableViewCell : UITableViewCell

@property (strong, nonatomic) Restaurant * restaurant;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end
