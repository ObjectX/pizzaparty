//
//  Restaurant.h
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Restaurant : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * formattedPhone;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lng;
@property (nonatomic, retain) NSNumber * distance;
@property (nonatomic, retain) NSString * formattedAddress;

@end
