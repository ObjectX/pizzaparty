//
//  PizzaRestaurantManager.m
//  PizzaParty
//
//  Created by Gagik Kyurkchyan on 8/31/15.
//  Copyright (c) 2015 ObjectX. All rights reserved.
//

#import "PizzaRestaurantManager.h"
#import "Constants.h"
#import "Restaurant.h"
#import "AppDelegate.h"


@interface PizzaRestaurantManager()

@property (strong, nonatomic) NSManagedObjectContext * managedObjectContext;
@property (strong, nonatomic) CLLocationManager * locationManager;
@property (strong, nonatomic) CLLocation * currentLocation;
@property (nonatomic) BOOL isLoading;

@end

@implementation PizzaRestaurantManager

static PizzaRestaurantManager *sharedInstance = nil;

+(PizzaRestaurantManager*)instance
{
    if(sharedInstance != nil)
        return sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super allocWithZone:NULL] init];
    });

    return sharedInstance;
}

#pragma mark - Public API
-(void)loadNearbyRestaurants
{
    if(self.locationManager == nil)
    {
        self.locationManager = [CLLocationManager new];
        self.locationManager.desiredAccuracy = 10;
        self.locationManager.delegate = self;
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startUpdatingLocation];
    }
}

-(NSFetchedResultsController*)getRestaurantsAsync:(Callback)callback
{
    __block NSFetchedResultsController * theFetchResultsController = nil;
    [self.managedObjectContext performBlockAndWait:^{
        NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Restaurant"];
        request.sortDescriptors = [[NSArray alloc] initWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES], nil];
        theFetchResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    }];
    [self.managedObjectContext performBlock:^{
        NSError * error;
        [theFetchResultsController performFetch:&error];
        if (error) {
            NSLog(@"Could not load retaurants. DETAILS \n %@",error);
        }
        
        if(callback != nil)
            dispatch_async(dispatch_get_main_queue(), ^{
                callback();
            });
    }];
    return theFetchResultsController;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{

    CLLocation * newLocation = [locations lastObject];
    
    if (self.currentLocation == nil || self.currentLocation.horizontalAccuracy >= newLocation.horizontalAccuracy) {
        self.currentLocation = newLocation;
        
        if (newLocation.horizontalAccuracy <= self.locationManager.desiredAccuracy) {
            [self.locationManager stopUpdatingLocation];
            if(!self.isLoading)
            {
                self.isLoading = YES;
                [self loadRestaurants];
            }
        }
    }
}

#pragma mark - Utility Methods

-(NSManagedObjectContext*)managedObjectContext
{
    if(_managedObjectContext == nil)
    {
        AppDelegate * del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = del.managedObjectContext;
    }
    return _managedObjectContext;
}

-(void)loadRestaurants
{
    __weak typeof(self) weakSelf = self;
    [self.managedObjectContext performBlock:^{
        
        //Delete previous entities
        NSFetchRequest *allRestaurants = [[NSFetchRequest alloc] init];
        [allRestaurants setEntity:[NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:weakSelf.managedObjectContext]];
        [allRestaurants setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError *error = nil;
        NSArray *restaurants = [weakSelf.managedObjectContext executeFetchRequest:allRestaurants error:&error];

        for (NSManagedObject *restaurant in restaurants) {
            [weakSelf.managedObjectContext deleteObject:restaurant];
        }
        NSError *saveError = nil;
        [self.managedObjectContext save:&saveError];
        
        //Fetch new entities
        [[NSNotificationCenter defaultCenter] postNotificationName:LoadingRestaurantsNotification object:nil];
        CLLocationCoordinate2D coord = weakSelf.currentLocation.coordinate;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/search?client_id=%@&client_secret=%@&v=20130815&ll=%f,%f&query=pizza", ForsquareClientID, ForsquareClientSecret, coord.latitude, coord.longitude]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 [weakSelf parseResponse:data];
             }
             else
             {
                 [weakSelf postCompletedNotification:NO];
             }
         }];
    }];
   
}

-(void)parseResponse:(NSData*)data
{
    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    NSArray * restaurants = [[json objectForKey:@"response"] objectForKey:@"venues"];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    for (NSDictionary * obj in restaurants) {
        Restaurant * r = (Restaurant *)[NSEntityDescription insertNewObjectForEntityForName:@"Restaurant" inManagedObjectContext:self.managedObjectContext];
        r.name = [obj objectForKey:@"name"];
        NSDictionary * contact = [obj objectForKey:@"contact"];
        r.formattedPhone = [contact objectForKey:@"formattedPhone"];
        
        NSDictionary * location = [obj objectForKey:@"location"];
        NSArray * address = [location objectForKey:@"formattedAddress"];
        r.formattedAddress = [address componentsJoinedByString:@"\n"];
        r.distance = [location objectForKey:@"distance"];
        r.lat = [location objectForKey:@"lat"];
        r.lng = [location objectForKey:@"lng"];
    }
    
    __weak typeof(self) weakSelf = self;
    [self.managedObjectContext performBlock:^{
        NSError * error = nil;
        if (![weakSelf.managedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            [weakSelf postCompletedNotification:false];
        }
        else
        {
            [weakSelf postCompletedNotification:true];
        }
    }];
}



-(void)postCompletedNotification:(BOOL)successful
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(successful)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:RestaurantsLoadedNotification object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:RestaurantsLoadingFailure object:nil];
        }
    });
}

@end
